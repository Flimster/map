import { Diagram } from "wasm-map-generator";

const X = 0;
const Y = 1;

const INITIAL_POINT_COUNT = 100;
const ITERATIONS = 0;

const c = document.getElementById("diagram")
const ctx = c.getContext("2d");

const pointSlider = document.getElementById("myRange");
const relaxationSlider = document.getElementById("lloyd");

const pointCounter = document.getElementById("point-counter");
const lloydRelaxtion = document.getElementById("lloyd-counter");
const pointsFlag = document.getElementById("points-flag");

class DiagramDrawer {
	constructor(diagram, ctx) {
		this.diagram = diagram;
		this.ctx = ctx;
		this.pointCount = INITIAL_POINT_COUNT;
		this.drawPointsFlag = true;
		this.relaxationIterations = 0;
	}

	// Draws the actual diagram
	draw() {
		ctx.clearRect(0,0, c.width, c.height);
		this.drawPolygons();
		if (this.drawPointsFlag) {
			this.drawPoints();
		}
	}

	// Draws where the points are on the diagram
	// Optional, can be toggled on or off
	drawPoints() {
		let points = this.diagram.get_points();
		ctx.fillStyle = "red";
		for (var point of points) {
			this.ctx.fillRect(point[X], point[Y], 5, 5);
		}
	}

	// Draws all the polygons
	drawPolygons() {
		let pointsPerPolygon = this.diagram.get_points_per_polygon();
		let polygonPoints = this.diagram.get_polygons();

		let start_point_index = 0;
		let end_point_index = 0;

		for (var pointCount of pointsPerPolygon) {
			end_point_index += pointCount;
			let point = polygonPoints.slice(start_point_index, end_point_index);
			this.drawPolygon(point);
			start_point_index += pointCount;
		}
	}

	// Draws a single polygon
	drawPolygon(points) {
		ctx.beginPath();
		//	ctx.fillStyle = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
		ctx.moveTo(points[0][X], points[0][Y]);
		for (var i = 1; i < points.length; i++) {
			ctx.lineTo(points[i][X], points[i][Y]);	
		}
		ctx.moveTo(points[points.length - 1][X], points[points.length - 1][Y]);
		ctx.lineTo(points[0][X], points[0][Y]);	
		ctx.stroke();
		// ctx.fill();
	}

	// Toggle on or off whether to draw the points of the diagram
	togglePoints() {
		this.drawPointsFlag = !this.drawPointsFlag;
	}
}


// Initial setup 

pointSlider.value = INITIAL_POINT_COUNT;
relaxationSlider.value = ITERATIONS;

let diagram = Diagram.new(INITIAL_POINT_COUNT, ITERATIONS);

c.width = diagram.get_width();
c.height = diagram.get_height();

let drawer = new DiagramDrawer(diagram, ctx);
drawer.draw();

// When the users changes how many points he wants
pointSlider.oninput = function() {
	diagram.generate_new_diagram(this.value);
	pointCounter.textContent = `Point count: ${this.value}`;
	drawer.draw();
}

// How many iterations of lloyds relaxation algorithm the user wants
relaxationSlider.oninput = function() {
 	diagram.set_relaxation_iterations(this.value);
	lloydRelaxtion.textContent = `Lloyd relaxation iterations: ${this.value}`
 	drawer.draw();
}
 
// Should the drawer draw the points of the diagram
pointsFlag.onclick = function() {
	drawer.togglePoints();
 	drawer.draw();
}
