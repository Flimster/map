extern crate js_sys;
extern crate voronoi;
extern crate web_sys;

#[macro_use]
extern crate serde_derive;

mod utils;

use voronoi::{lloyd_relaxation, make_polygons, voronoi, Point};
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

const BOX_SIZE: f64 = 850.;
const DIMENSION: usize = 850;

// A macro to provide `println!(..)`-style syntax for `console.log` logging.
macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}

#[wasm_bindgen]
pub struct Diagram {
    width: usize,
    height: usize,
    point_count: usize,
    relaxation_iterations: usize,
    vor_pts: Vec<Point>,
    vor_pts_default: Vec<Point>,
    polygons: Vec<Vec<Point>>,
}

#[wasm_bindgen]
impl Diagram {
    /// Creates a new voronai Diagram that
    pub fn new(point_count: usize, relaxation_iterations: usize) -> Diagram {
        let width = DIMENSION;
        let height = DIMENSION;

        let vor_pts_default = generate_points(point_count);
        let vor_pts = vor_pts_default.clone();

        let vor_diagram = voronoi(vor_pts_default.clone(), BOX_SIZE);
        let polygons = make_polygons(&vor_diagram);

        Diagram {
            width,
            height,
            point_count,
            relaxation_iterations,
            vor_pts,
            vor_pts_default,
            polygons,
        }
    }

    /// Returns the width of the diagram
    pub fn get_width(&self) -> usize {
        self.width
    }

    /// Returns the height of the diagram
    pub fn get_height(&self) -> usize {
        self.height
    }

    /// Given a number of points, this generates a new voronoi diagram
    /// from scratch
    pub fn generate_new_diagram(&mut self, point_count: usize) {
        self.point_count = point_count;
        self.vor_pts_default = generate_points(self.point_count);
        self.generate_diagram();
    }

    /// Sets how many times Lloys algorithm should be applied to the diagram
    /// In using this function, the diagram itself changes and so the diagram 
    /// before and after using this function are different
    pub fn set_relaxation_iterations(&mut self, iterations: usize) {
        self.relaxation_iterations = iterations;
        self.generate_diagram();
    }

    // Internal api to make it easier to generate a new diagram
    fn generate_diagram(&mut self) {
        let mut vor_pts = self.vor_pts_default.clone();

        for _i in 0..self.relaxation_iterations {
            vor_pts = lloyd_relaxation(vor_pts, BOX_SIZE);
        }

        self.vor_pts = vor_pts.clone();
        let vor_diagram = voronoi(vor_pts.clone(), BOX_SIZE);
        self.polygons = make_polygons(&vor_diagram);
    }

    /// Use it in javascript to get the coordiantes of the points
    /// used in the diagram
    pub fn get_points(&self) -> JsValue {
        let mut points = Vec::new();
        for point in &self.vor_pts {
            points.push((point.x.into_inner() as usize, point.y.into_inner() as usize));
        }

        JsValue::from_serde(&points).unwrap()
    }

    /// Gets the coordinate for each endpoint in the polygon
    pub fn get_polygons(&self) -> JsValue {
        let mut points = Vec::new();

        for polygon in &self.polygons {
            for point in polygon {
                let coordinates = (point.x.into_inner() as usize, point.y.into_inner() as usize);
                points.push(coordinates);
            }
        }

        JsValue::from_serde(&points).unwrap()
    }

    /// Gets how many endpoints there are for each polygon
    pub fn get_points_per_polygon(&self) -> JsValue {
        let mut num_of_points = Vec::new();
        for polygon in &self.polygons {
            num_of_points.push(polygon.len());
        }
        JsValue::from_serde(&num_of_points).unwrap()
    }
}

// A function that randomly generates points and places them on the diagram
fn generate_points(num_of_points: usize) -> Vec<Point> {
    let mut vor_pts = Vec::new();
    for _i in 0..num_of_points {
        let x = js_sys::Math::random() * BOX_SIZE;
        let y = js_sys::Math::random() * BOX_SIZE;
        vor_pts.push(Point::new(x, y));
    }

    vor_pts
}
